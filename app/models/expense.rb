class Expense < ActiveRecord::Base
  attr_accessible :cost, :frequency, :name

  def dailycost
    cost / frequency
  end

  def self.totalExpense
    Expense.sum("cost / frequency")
  end

end
