class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.string :name
      t.float :amount
      t.integer :frequency

      t.timestamps
    end
  end
end
