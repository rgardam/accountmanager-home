class ChangeNameInExpenses < ActiveRecord::Migration
  def up
    change_column :expenses, :name, :string
  end

  def down
  end
end
