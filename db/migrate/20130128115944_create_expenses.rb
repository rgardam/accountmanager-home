class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.text :name
      t.float :cost
      t.integer :frequency

      t.timestamps
    end
  end
end
